//
//  LinkedLists.h
//  CommandLineTool
//
//  Created by Thomas Sanger Borthwick on 10/9/14.
//  Copyright (c) 2014 Tom Mitchell. All rights reserved.
//

#ifndef __CommandLineTool__LinkedLists__
#define __CommandLineTool__LinkedLists__

#include <iostream>

class LinkedList
{

public:
 
    LinkedList();
    
    ~LinkedList();
    
    void add(float itemValue);
    
    float get(int index);
    
    int size();
    

private:
    
    struct Node
    {
        float value;
        Node* next;
    };
    
    Node* head;
    
};

#endif /* defined(__CommandLineTool__LinkedLists__) */
