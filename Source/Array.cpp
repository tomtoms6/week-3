//
//  Array.cpp
//  CommandLineTool
//
//  Created by Thomas Sanger Borthwick on 10/9/14.
//  Copyright (c) 2014 Tom Mitchell. All rights reserved.
//

#include "Array.h"

Array::Array()              //constructor
{
    
    arrPoint = new float[0];
}

Array::~Array()             //destructor
{
    delete[] arrPoint;
}

void Array::add(float itemValue)       //mod
{
    float* newArr = nullptr;   //create array to copy to
    
    if (newArr == nullptr) {
        newArr = new float[arrSize+1];
    }
    
    for (int i = 0; i < arrSize ; i++) {    //copy
       newArr[i] = arrPoint[i];
    }
    
    newArr[arrSize] = itemValue;           //add new value to end
    
    delete[] arrPoint;                      //delete original
    
    arrPoint = newArr;
                          
    newArr = nullptr;

    arrSize++;  

}

float Array::get(int index) const      //access
{
    ///if (index <= arrSize && index > 0)
    return arrPoint[index];
    
   // else
   //     return 0;
}

int Array::size() const                //access
{
    return arrSize;
}