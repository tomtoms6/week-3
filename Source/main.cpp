//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include "Array.h"
#include "LinkedList.h"

bool testArray(); //function to test Array class
bool testLinkedList(); //function to test LinkedList class

int main (int argc, const char* argv[])
{
    
    if (testArray() == true)
    {
        std::cout << "all Array tests passed\n";
    }
    else
    {
        std::cout << "Array tests failed\n";
    }
    
    if (testLinkedList() == true)
    {
        std::cout << "all LinkedList tests passed\n";
    }
    else
    {
        std::cout << "Array LinkedList failed\n";
    }
    
    return 0;
}

bool testArray()
{
    Array array;
    const float testArray[] = {0.f, 1.f, 2.f, 3.f, 4.f, 5.f};
    const int testArraySize = sizeof (testArray) / sizeof (testArray[0]);
    
    if (array.size() != 0)
    {
        std::cout << "size is incorrect\n";
        return false;
    }
    
    for (int i = 0; i < testArraySize; i++)
    {
        array.add (testArray[i]);
        
        if (array.size() != i + 1)
        {
            std::cout << "size is incorrect\n";
            return false;
        }
        
        if (array.get (i) != testArray[i])
        {
            std::cout << "value at index "<< i << " recalled incorrectly\n" ;
            return false;
        }
    }
    //    //removing first
    //    array.remove (0);
    //    if (array.size() != testArraySize - 1)
    //    {
    //        std::cout << "with size after removing item\n";
    //        return false;
    //    }
    //
    //    for (int i = 0; i < array.size(); i++)
    //    {
    //        if (array.get(i) != testArray[i+1])
    //        {
    //            std::cout << "problems removing items\n";
    //            return false;
    //        }
    //    }
    //
    //    //removing last
    //    array.remove (array.size() - 1);
    //    if (array.size() != testArraySize - 2)
    //    {
    //        std::cout << "with size after removing item\n";
    //        return false;
    //    }
    //
    //    for (int i = 0; i < array.size(); i++)
    //    {
    //        if (array.get(i) != testArray[i + 1])
    //        {
    //            std::cout << "problems removing items\n";
    //            return false;
    //        }
    //    }
    //
    //    //remove second item
    //    array.remove (1);
    //    if (array.size() != testArraySize - 3)
    //    {
    //        std::cout << "with size after removing item\n";
    //        return false;
    //    }
    //
    //    if (array.get (0) != testArray[1])
    //    {
    //        std::cout << "problems removing items\n";
    //        return false;
    //    }
    //
    //    if (array.get (1) != testArray[3])
    //    {
    //        std::cout << "problems removing items\n";
    //        return false;
    //    }
    //    
    //    if (array.get (2) != testArray[4])
    //    {
    //        std::cout << "problems removing items\n";
    //        return false;
    //    }
    
    
    return true;
}

bool testLinkedList()
{
    LinkedList list;
    const float testArray[] = {0.f, 1.f, 2.f, 3.f, 4.f, 5.f};
    const int testArraySize = sizeof (testArray) / sizeof (testArray[0]);
    
    if (list.size() != 0)
    {
        std::cout << "size is incorrect\n";
        return false;
    }
    
    for (int i = 0; i < testArraySize; i++)
    {
        list.add (testArray[i]);
        
        if (list.size() != i + 1)
        {
            std::cout << "size is incorrect\n";
            return false;
        }
        
        if (list.get (i) != testArray[i])
        {
            std::cout << "value at index "<< i << " recalled incorrectly\n" ;
            return false;
        }
    }
    //    //removing first
    //    list.remove (0);
    //    if (list.size() != testArraySize - 1)
    //    {
    //        std::cout << "with size after removing item\n";
    //        return false;
    //    }
    //
    //    for (int i = 0; i < list.size(); i++)
    //    {
    //        if (list.get(i) != testArray[i+1])
    //        {
    //            std::cout << "problems removing items\n";
    //            return false;
    //        }
    //    }
    //
    //    //removing last
    //    list.remove (list.size() - 1);
    //    if (list.size() != testArraySize - 2)
    //    {
    //        std::cout << "with size after removing item\n";
    //        return false;
    //    }
    //
    //    for (int i = 0; i < list.size(); i++)
    //    {
    //        if (list.get(i) != testArray[i + 1])
    //        {
    //            std::cout << "problems removing items\n";
    //            return false;
    //        }
    //    }
    //
    //    //remove second item
    //    list.remove (1);
    //    if (list.size() != testArraySize - 3)
    //    {
    //        std::cout << "with size after removing item\n";
    //        return false;
    //    }
    //
    //    if (list.get (0) != testArray[1])
    //    {
    //        std::cout << "problems removing items\n";
    //        return false;
    //    }
    //
    //    if (list.get (1) != testArray[3])
    //    {
    //        std::cout << "problems removing items\n";
    //        return false;
    //    }
    //
    //    if (list.get (2) != testArray[4])
    //    {
    //        std::cout << "problems removing items\n";
    //        return false;
    //    }
    
    
    return true;
}