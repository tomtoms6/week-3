//
//  Array.h
//  CommandLineTool
//
//  Created by Thomas Sanger Borthwick on 10/9/14.
//  Copyright (c) 2014 Tom Mitchell. All rights reserved.
//

#ifndef __CommandLineTool__Array__
#define __CommandLineTool__Array__

#include <iostream>

class Array
{
public:
    
    /** Constructor, Initialise data members */
    Array();
    
    /** Destructor, deletes allocated memory*/
    ~Array();
    
    /**add new item to the end of the array*/
    void add(float itemValue);
    
    /** returns the item at the index*/
    float get(int index) const;
    
    /** returns the number of items currently in the array*/
    int size() const;
    
private:
    
    int arrSize = 0;
    float* arrPoint = nullptr;
    
};


#endif /* defined(__CommandLineTool__Array__) */
